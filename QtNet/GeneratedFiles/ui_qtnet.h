/********************************************************************************
** Form generated from reading UI file 'qtnet.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTNET_H
#define UI_QTNET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_QtNetClass
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QPushButton *pushButton;
    QListWidget *listWidget;

    void setupUi(QDialog *QtNetClass)
    {
        if (QtNetClass->objectName().isEmpty())
            QtNetClass->setObjectName(QStringLiteral("QtNetClass"));
        QtNetClass->resize(446, 495);
        verticalLayout = new QVBoxLayout(QtNetClass);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(-1, -1, -1, 0);
        pushButton = new QPushButton(QtNetClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(0, QFormLayout::LabelRole, pushButton);


        verticalLayout->addLayout(formLayout);

        listWidget = new QListWidget(QtNetClass);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);


        retranslateUi(QtNetClass);
        QObject::connect(pushButton, SIGNAL(clicked()), QtNetClass, SLOT(stiahni()));

        QMetaObject::connectSlotsByName(QtNetClass);
    } // setupUi

    void retranslateUi(QDialog *QtNetClass)
    {
        QtNetClass->setWindowTitle(QApplication::translate("QtNetClass", "QtNet", 0));
        pushButton->setText(QApplication::translate("QtNetClass", "Stiahni", 0));
    } // retranslateUi

};

namespace Ui {
    class QtNetClass: public Ui_QtNetClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTNET_H
