#ifndef QTNET_H
#define QTNET_H

#include <QtWidgets/QDialog>
#include "ui_qtnet.h"
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork>

class QtNet : public QDialog
{
	Q_OBJECT

public:
	QtNet(QWidget *parent = 0);
	~QtNet();
public slots:
	void stiahni();
	void citaj_stranku();
private:
	Ui::QtNetClass ui;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;
};

#endif // QTNET_H
