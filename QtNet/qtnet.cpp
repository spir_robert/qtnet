#include "qtnet.h"

QtNet::QtNet(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

QtNet::~QtNet()
{

}

void QtNet::stiahni()
{
	url.setUrl(QString("http://gauss.math.sk/studenti.txt"));
	reply = qnam.get(QNetworkRequest(url));
	connect(reply, &QNetworkReply::finished, this, &QtNet::citaj_stranku);
}

void QtNet::citaj_stranku()
{
	QString vysledok = reply->readAll();
	ui.listWidget->addItems(vysledok.split(','));
}